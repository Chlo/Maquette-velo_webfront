import BestsellersItem from './BestsellersItem';
class Bestsellers{
    constructor(el){
        this.el = el;
        this.items = [];
    };

    init(url) {
        //définition du conteneur du contenu
        this.container = this.el.querySelector('.list-article__ul');

        //Chargement du json
        this.load(url);
    };

    load(url) {
        const req = new XMLHttpRequest();
        //overture du fichier json
        req.open('GET', url, true);
        //renvoi req sur la fonction loaded pour verifier s'il est chargé
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();
    };

    loaded(req) {
        if (req.readyState === 4) {
            if (req.status === 200) {
                //après vérifiation du chargement du fichier on traite les données
                this.build(JSON.parse(req.responseText));
            }
        }
    };

    build(datas) {
        let i = 0;
        let item;

        //nb de produit généré en fonction du la taille de l'ecran
        if (window.innerWidth < 1024 && window.innerWidth >= 840) {
            this.max = 4;
        } else if (window.innerWidth < 840 && window.innerWidth >= 655){
            this.max = 3;
        } else if (window.innerWidth < 655){
            this.max = 4;
        } else {
            this.max = 5;
        }

        for (i; i < this.max; i++) {
            item = new BestsellersItem(i, datas[i]);
            this.container.appendChild(item.build());
            this.items.push(item);
        }

    };
}

export default Bestsellers;