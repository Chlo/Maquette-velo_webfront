import {BUS} from './BUS';
import SliderItem from './SliderItem';
import SliderBullet from './SliderBullet';

class Slider {

    constructor(el){
        this.el = el;
        this.items = [];
        this.current = 0;
        this.max = 0;
    };

    init(url) {
        //définition du conteneur du contenu
        this.container = this.el.querySelector('.slider__wrapper');

        //définition des controls
        this.left = this.el.querySelector('.slider__arrow--left');
        this.right = this.el.querySelector('.slider__arrow--right');

        //Chargement du json
        this.load(url);

        // Lance l'écoute
        this.listen();
    };

    load(url) {
        const req = new XMLHttpRequest();
        //overture du fichier json
        req.open('GET', url, true);
        //renvoi req sur la fonction loaded pour verifier s'il est chargé
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();
    };

    loaded(req) {
        if (req.readyState === 4) {
            if (req.status === 200) {
                //après vérifiation du chargement du fichier on traite les données
                this.build(JSON.parse(req.responseText));
            }
        }
    };

    build(datas) {
        //nombre max d'objets
        this.max = datas.length;
        let bullets = this.el.querySelector('.slider__bullets');

        let i = 0;
        let item;
        let bullet;

        for (i; i < this.max; i++) {
            //construction des slides
            item = new SliderItem(i, datas[i]);
            this.container.appendChild(item.build());
            this.items.push(item);

            //construction des bullets
            bullet = new SliderBullet(i, datas[i]);
            bullets.appendChild(bullet.build());

            //cacher les slides et laisser afficher seulement la premiere
            if (item.id !== this.current) {
                item.hide();
            }
            if (item.id === this.current){
                item.el.classList.add('slides--current');
                item.show();
            }

        }
    };

    listen() {
        //ecoute des controleurs
        this.left.addEventListener('click', dir => this.move(-1));
        this.right.addEventListener('click', dir => this.move(1));

        //ecoute aussi des bullets
        BUS.listen('bulletMove', this.bulletMove, this);
    };

    bulletMove(e) {
        //actionner le movement du slideur grace au bullets
        this.move(e.detail.bullet - this.current);
    };

    move(dir) {
        //on cache la slide active
        this.items[this.current].hide(dir);
        //on defini la prochaine slide active
        this.current = this.current + dir;

        // si on est à la dernière slide -> aller a la permière
        if (this.current >= this.max) {
            this.current = 0;
            this.items[this.current].show(dir);

        // si on est à la première -> on affiche la dernière
        } else if (this.current < 0) {
            this.current = this.max - 1;
            this.items[this.current].show(dir);
        } else {
            this.items[this.current].show(dir);
        }
    };
}
 export default Slider;
