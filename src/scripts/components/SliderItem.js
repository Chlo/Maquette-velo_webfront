import {TweenMax} from 'gsap';
import {TimeLineMax} from 'gsap';
class SliderItem{
    constructor(id, datas){
        this.id = id;
        this.datas = datas;
    };

    build() {
        this.el = document.createElement('div');
        this.el.classList.add("slides");

        // Process datas
        this.el.classList.add("imgID__" + this.id);
        this.el.innerHTML = '' +
            '<img class="slides__img" src="' + this.datas.img + '" width="350"/>' +
            '<div class="slides__caption">' +
            '<h1 class="slides__caption__h1">' + this.datas.title + '</h1>' +
            '<p class="slides__caption__p">' + this.datas.desc + '</p>' +
            '<button class="button button--slides__caption" ><a href="' + this.datas.url + '">Commander <img src="./images/arrow_buttons.png" /><!--<i class="fa fa-chevron-right" aria-hidden="true"></i>--></a></button>' +
            '</div>';

        return this.el;
    };

    show(dir) {
        this.el.classList.add('slides--current');

        let image = this.el.children[0];
        image.style.position = "relative";
        let content = this.el.children[1];
        content.style.position = "relative";
        let text = this.el.children[1].children[0];
        text.style.position = "relative";
        let text1 = this.el.children[1].children[1];
        text1.style.position = "relative";
        let button = this.el.children[1].children[2];
        button.style.position = "relative";

        //animation de tout le block de contenu
        var tweenContent = TweenMax.fromTo(content, 1, {autoAlpha:0}, {autoAlpha:1});
        tweenContent.delay(0.5);
        //animation du text
        var tweenText = TweenMax.fromTo(text, 1, {top: -20}, {top: 0});
        tweenText.delay(0.5);
        var tweenText1 = TweenMax.fromTo(text1, 1, {top: -20}, {top: 0});
        tweenText1.delay(0.5);
        //animation button
        var tweenButton = TweenMax.fromTo(button, 1, {top: 51}, {top: 31});
        tweenButton.delay(0.5);


        let bullet = document.body.querySelector('.bullet_'+this.id+' img');
        TweenMax.to(bullet, 2, {width: 48, borderColor: '#000'});

        if (dir === -1) {
            if (window.innerWidth > 1024) {
                //animation image -> gauche
                let tweenImage = TweenMax.fromTo(image, 1, {left: 400, autoAlpha: 0, 'box-shadow': '5px 5px 20px'}, {left: 30, autoAlpha: 1, 'box-shadow': '0px 0px 0px'});
                tweenImage.delay(0.5);
            } else {
                let tweenImage = TweenMax.fromTo(image, 1, {left: 100, autoAlpha: 0, 'box-shadow': '5px 5px 20px'}, {left: 0, autoAlpha: 1, 'box-shadow': '0px 0px 0px'});
                tweenImage.delay(0.5);
            }
        } else {
            if (window.innerWidth > 1024) {
                //Animation image -> droite
                let tweenImage = TweenMax.fromTo(image, 1, {left: -100, autoAlpha: 0, 'box-shadow': '5px 5px 20px'}, {left: 30, autoAlpha: 1, 'box-shadow': '0px 0px 0px'});
                tweenImage.delay(0.5);
            } else {
                let tweenImage = TweenMax.fromTo(image, 1, {left: -100, autoAlpha: 0, 'box-shadow': '5px 5px 20px'}, {left: 0, autoAlpha: 1, 'box-shadow': '0px 0px 0px'});
                tweenImage.delay(0.5);
            }
        }
    };

    hide(dir) {

        let image = this.el.children[0];
        image.style.position = "relative";
        let content = this.el.children[1];
        content.style.position = "relative";
        let text = this.el.children[1].children[0];
        text.style.position = "relative";
        let text1 = this.el.children[1].children[1];
        text1.style.position = "relative";
        let button = this.el.children[1].children[2];
        button.style.position = "relative";

        //animation de tout le block de contenu
        TweenMax.fromTo(content, 1, {autoAlpha:1}, {autoAlpha:0});
        //animation du text
        TweenMax.fromTo(text, 1, {top: 0}, {top: -20});
        TweenMax.fromTo(text1, 1, {top: 0}, {top: -20});
        //animation button
        TweenMax.fromTo(button, 1, {top: 31}, {top: 51});

        let bullet = document.body.querySelectorAll('.bullet img');

        TweenMax.to(bullet, 2, {width: 38, borderColor: '#fff'});

        if (dir === -1) {
            if (window.innerWidth > 1024) {
                // Animation image -> gauche
                TweenMax.fromTo(image, 1, {left: 30, autoAlpha: 1}, {left: -100, autoAlpha: 0});
            } else {
                TweenMax.fromTo(image, 1, {left: 0, autoAlpha: 1}, {left: -100, autoAlpha: 0});
            }
        } else {
            if (window.innerWidth > 1024) {
                // Animation image -> droite
                TweenMax.fromTo(image, 1, {left: 30, autoAlpha: 1}, {left: 400, autoAlpha: 0});
            } else {
                TweenMax.fromTo(image, 1, {left: 0, autoAlpha: 1}, {left: 100, autoAlpha: 0});
            }
        }

        this.el.classList.remove('slides--current');
    };
}

export default SliderItem;