import {BUS} from './BUS';
class SliderBullet {
    constructor(id, datas){
        this.id = id;
        this.datas = datas;
    };

    build() {
        this.el = document.createElement('li');
        this.el.classList.add('bullet', 'bullet_'+this.id);

        // Process datas
        this.el.innerHTML = '<img class="slider__bullets__thumb" src="' + this.datas.thumb + '"/>';

        this.el.addEventListener('click', evt => this.click(evt));
        return this.el;
    };

    click() {
        BUS.dispatch('bulletMove', {detail: {bullet: this.id}});
    };
}
export default SliderBullet;