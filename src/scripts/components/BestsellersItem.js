class BestsellersItem{
    constructor(id, datas){
        this.id = id;
        this.datas = datas;
    };

    build() {

        this.el = document.createElement('li');

        // Process datas
        this.el.classList.add('list-article__li', 'list-article__li--article-' + this.id);
        this.el.innerHTML = '' +
            '<a class="list-article__a">' +
            '<article class="list-article__article" data-popularite="' + this.datas.popularite + '" data-price="' + this.datas.price + '" >' +
            '<img class="list-article__article__img" src="' + this.datas.img + '"  width="180"/>' +
            '<h1>' + this.datas.name + '</h1>' +
            '<p class="list-article__article__detail">' + this.datas.color + '</p>' +
            '<span class="list-article__article__stock">' + this.datas.stock + ' en stock</span>' +
            '</article>' +
            '</a>';

        // ajout de la classe qui donne la couleur orrange sur les articles qui ont un stovk faible
        if(this.datas.stock <= 1){
            this.el.querySelector('.list-article__article__stock').classList.add('list-article__article__stock--low');
        } else {
            this.el.querySelector('.list-article__article__stock').classList.remove('list-article__article__stock--low');
        }

        return this.el;
    };
}

export default BestsellersItem;
