import Slider from './components/Slider';
import Bestsellers from './components/Bestsellers';
import All_Products from './components/All_Products';
import SelectBestsellers from './components/Select';

//Récupération de l'element .slider
let slider = new Slider(document.querySelector(".slider"));
//fonction ini sur l'el avec l'url du fichier json
slider.init('./datas/datas_slider.json');


let bestsellers = new Bestsellers(document.querySelector('.bestsellers'));
bestsellers.init('./datas/datas_products');

let all_products = new All_Products(document.querySelector('.all-products'));
all_products.init('./datas/datas_products');

// SelectBestsellers();


if (window.innerWidth < 768){

    var ham = document.body.querySelector('.header-principal__nav--right');
    console.log(ham);
    ham.classList.add('infos-hidden');

    var hist = document.body.querySelector('.infos__historique');
    var listHist = hist.querySelector('.info__ul');
    listHist.classList.add('infos-hidden');
    var btn = document.body.querySelector('.button--historique');
    btn.classList.add('infos-hidden');
    var historique = hist.querySelector( '.infos__title' );
    historique.onclick = function() {
        listHist.classList.toggle('infos-hidden');
        btn.classList.toggle('infos-hidden');
    };

    var forma = document.body.querySelector('.infos__formations');
    var listForm = forma.querySelector('.info__ul');
    listForm.classList.add('infos-hidden');
    var formations = forma.querySelector('.infos__title');
    formations.onclick = function() {
        listForm.classList.toggle('infos-hidden');
    };
}